function onClick() {
    let f1 = document.getElementById("cost");
    let f2 = document.getElementById("quantity");
    let r = document.getElementById("result");
    if ((f1.value === "") || (f2.value === "")) {
        r.innerHTML = 0;
    } else {
        if (((/^(0|[1-9][0-9]*)$/).test(f1.value)) && ((/^(0|[1-9][0-9]*)$/).test(f2.value))) {
            r.innerHTML = f1.value * f2.value;
        } else {
            if (!(/^(0|[1-9][0-9]*)$/).test(f1.value)) {
                alert("Введите корректную стоимость");
                f1.value = "";
            }
            if (!(/^(0|[1-9][0-9]*)$/).test(f2.value)) {
                alert("Введите корректное число заявок");
                f2.value = "";
            }
            r.innerHTML = 0;
        }
    }
}

window.addEventListener("DOMContentLoaded", function(event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("my-button");
    b.addEventListener("click", onClick);
});